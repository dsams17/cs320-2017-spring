(* ****** ****** *)
//
// HX:
// How to compile:
// patscc -o assign02_sol assign02_sol.dats
//
// How to test it:
// ./assign02_sol
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign02.dats"

(* ****** ****** *)

(*
implement
show_triangle(n) = ...
*)

(* ****** ****** *)

(*
implement
print_digit_?
*)

(* ****** ****** *)

(*
implement
print_digit_?
*)

(* ****** ****** *)

(*
implement MS(n) = ...
*)

(* ****** ****** *)
//
// HX-2017-01-29:
// Please do not modify the following code
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val () = show_triangle(3)
val () = show_triangle(5)
//
(*
val () = print_digit_?() // you need to change ?
val () = print_digit_?() // you need to change ? 
*)
//
val () = println!("MS(1000) = ", MS(1000))
val () = println!("MS(10000) = ", MS(10000))
val () = println!("MS(100000) = ", MS(100000))
val () = println!("MS(1000000) = ", MS(1000000))
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign02_sol.dats] *)
